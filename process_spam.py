from sklearn.preprocessing import normalize
import scipy.io as sio
import csv
import numpy as np

def main():
    data = sio.loadmat("spam_data.mat")
    print(data)

    training_data = data['training_data']
    test_data = data['test_data']
    training_labels = data['training_labels']

    print(training_data)

    n = training_data.shape[0]
    d = training_data.shape[1]

    print(training_data.shape)
    print(training_labels.shape)

    training_data = normalize(training_data.astype(float), norm='l2')

    training_data = np.append(training_data, training_labels.T,1)

    print(n, d)

    with open('spam_training.csv', 'w') as fout:
        writer = csv.writer(fout)

        for i in range(0, n):
            #for j in range(0,d):
            #    training_data[i][j] = int(training_data[i][j])
            writer.writerow(training_data[i].astype(int))

if __name__ == "__main__":
    main()

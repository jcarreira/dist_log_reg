CXXFLAGS = -ansi -Wall -Werror -std=c++14 -ggdb -fopenmp -O3 -march=corei7-avx -mtune=corei7-avx -funroll-loops
SOURCES  = main.cpp
INCLUDES = -Ithird_party

all: log_reg


log_reg: $(SOURCES)
	mpic++ $(CXXFLAGS) $(INCLUDES) main.cpp -o log_reg

run:
	mpirun -n 10 log_reg


clean:
	rm -rf log_reg a.out

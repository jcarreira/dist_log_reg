#include <cstdio>
#include <iostream>
#include <mpi.h>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <vector>
#include <cmath>
#include <utility>
#include <fstream>
//#include <Eigen/Dense>
#include "Utils.h"
#include <mpi.h>

//using namespace Eigen;

void check_mpi_error(int err) {
    if (err != MPI_SUCCESS) {
        exit(-1);
    }
}

class Tensor {
public:
    Tensor(uint64_t dim = 0) {
        data.resize(dim);
        fill(0);
    }

    void fill(double x) {
        std::fill(data.begin(), data.end(), x);
    }

    Tensor mult_const(double c) const {
        Tensor res(size());
        for (uint64_t i = 0; i < size();++i) {
            res.data[i] = c * data[i];
        }
        return res;
    }

    Tensor neg() const {
        return mult_const(-1);
    }

    Tensor sub(const Tensor& t) const {
        if (t.size() != size()) {
            throw std::runtime_error("sub on different sizes");
        }

        Tensor res(size());
        for (uint64_t i = 0; i < size();++i) {
            res.data[i] = data[i] - t.data[i];
        }

        return res;
    }
    
    Tensor add(const Tensor& t) const {
        return sub(t.neg());
    }
    
    Tensor add(double v) const {
        Tensor res(size());
        for (uint64_t i = 0; i < size(); ++i)
            res.data[i] = data[i] + v;
        return res;
    }

    uint64_t size() const {
        return data.size();
    }

public:
    std::vector<double> data;
};

class Matrix {
public:
    Matrix(const std::vector<Tensor>& m = std::vector<Tensor>()) :
        data(m)
    {
        //std::cout << "m.n: " << m.size() << " m.d: " << m[0].data.size() << std::endl;
        //std::cout << "data.n: " << data.size() << " m.d: " << data[0].data.size() << std::endl;
        cached_data = nullptr;
    }

    const Tensor& line(uint64_t l) const {
        return data[l];
    }

    Matrix T() const {
        if (cached_data != nullptr)
            return *cached_data;

        std::vector<Tensor> res;
        res.resize(cols());

        for (auto& l : res) {
            l.data.resize(rows());
        }

#pragma omp parallel for
        for (uint64_t l = 0; l < rows(); ++l) {
            for (uint64_t c = 0; c < cols(); ++c) {
                res[c].data[l] = data[l].data[c];
            }
        }

        //t_cached = true;
        cached_data = new Matrix(res);
        return *cached_data;
    }

    uint64_t rows() const {
        return data.size();
    }

    uint64_t cols() const {
        return data[0].size();
    }

public:
    std::vector<Tensor> data;
    mutable Matrix *cached_data;
};


class Dataset {
public:
    Dataset(const Matrix& m,
            const Tensor& l) :
        matrix(m),
    labels(l) {
    }

    uint64_t samples() const {
        return matrix.rows();
    }

    uint64_t features() const {
        return matrix.cols();
    }

public:
    Matrix matrix;
    Tensor labels;
};

auto read_input(const std::string& input_file) {
    std::cout << "Reading input file: " << input_file << std::endl;

    std::ifstream fin(input_file, std::ifstream::in);
    if (!fin)
        throw std::runtime_error("Error opening input file");

    std::vector<Tensor> samples;
    Tensor labels;

    std::string line;
    while (getline(fin, line)) {
        char* nl = strdup(line.c_str());
        char* s = nl;

        Tensor sample;
        while (char* l = strsep(&s, ",")) {
            double v = string_to<double>(l);
            sample.data.push_back(v);
        }
        free(nl);

        if (sample.data.back() != 1 && sample.data.back() != 0)
            throw std::runtime_error("Label must be 0 or 1");

        labels.data.push_back(sample.data.back());
        sample.data.pop_back();
        samples.push_back(sample);
    }

    return Dataset(samples, labels);
}

double dot_1d(const Tensor& t1, const Tensor& t2) {
    double res = 0;
    if (t1.size() != t2.size()) {
        throw std::runtime_error("Dot product on different sizes");
    }

    for (uint64_t i = 0; i < t1.size(); ++i) {
        res += t1.data[i] * t2.data[i];
    }
    return res;
}

double s_1(double x) {
    double res = 1.0 / (1.0 + exp(-x)); // pray and hope this doesn't overflow?
    if (std::isnan(res))
        throw std::runtime_error("s_1 generated nan");

    return res;
}

Tensor s_1d(const Tensor& t) {
    Tensor res(t.size());
    for (uint64_t i = 0; i < t.size(); ++i)
        res.data[i] = s_1(t.data[i]);
    return res;
}

const Tensor& dataset_line(Dataset& d, uint64_t line) {
    return d.matrix.line(line);
}

double calc_cost(Dataset& dataset, const Tensor& w) {
    double total_cost = 0;

#pragma omp parallel for num_threads(4)
    for (uint64_t i = 0; i < dataset.samples(); ++i) {
        double value = dataset.labels.data[i] * 
            log(s_1(dot_1d(dataset_line(dataset,i), w))) + 
            (1 - dataset.labels.data[i]) * log(1 - s_1(dot_1d(dataset_line(dataset, i), w)));

#pragma omp critical
        {
            total_cost -= value;
        }
    }

    if (std::isnan(total_cost))
        throw std::runtime_error("calc_cost generated nan");
    return total_cost;
}

Tensor dot_2d(const Matrix& m, const Tensor& t) {
    // final size is going to be m.n
    Tensor res(m.rows());

#pragma omp parallel for
    for (uint64_t i = 0; i < m.rows(); ++i) {
        for (uint64_t j = 0; j < m.cols(); ++j) {
            res.data[i] += m.data[i].data[j] * t.data[j];
        }
    }
    return res;
}

Tensor calc_stochastic_grad(const Dataset& dataset, const Tensor& w) {
    double epsilon = 0.0001;

    uint64_t x_index = rand() % dataset.samples();
    const Tensor& x = dataset.matrix.line(x_index);
    auto part1 = s_1(dot_1d(x, w)); // scalar
    //std::cout << "part1 size: " << part1.size() << std::endl;
    auto part2 = dataset.labels.data[x_index] - part1; // tensor
    //std::cout << "part2 size: " << part2.size() << std::endl;
    auto part3 = x.mult_const(part2);
    //std::cout << "part3 size: " << part3.size() << std::endl;
    //std::cout << "w size: " << w.size() << std::endl;
    auto part4 = w.mult_const(2 * epsilon);
    //std::cout << "part4 size: " << part4.size() << std::endl;
    auto res = part4.add(part3);
    //auto res = part4.sub(part3.neg());

    //std::cout << "Gradient size: " << res.size() << std::endl;

    return res;
}

Tensor calc_batch_grad(const Dataset& dataset, const Tensor& w) {
    double epsilon = 0.0001;
    auto part1 = s_1d(dot_2d(dataset.matrix, w)); // tensor
    auto part2 = dataset.labels.sub(part1); // tensor
    //std::cout << "part2 size: " << part2.size() << std::endl;
    auto part3 = dot_2d(dataset.matrix.T(), part2); // matrix x tensor = tensor
    //std::cout << "part3 size: " << part3.size() << std::endl;
    //std::cout << "w size: " << w.size() << std::endl;
    auto part4 = w.mult_const(2 * epsilon);
    //std::cout << "part4 size: " << part4.size() << std::endl;
    auto res = part4.add(part3);
    //auto res = part4.sub(part3.neg());

    //std::cout << "Gradient size: " << res.size() << std::endl;

    return res;
}

Tensor update_w(const Tensor& w, double learning_rate, const Tensor& grad) {
    //std::cout << w.size() << std::endl;
    //std::cout << grad.size() << std::endl;
    return w.add(grad.mult_const(learning_rate));
    //return w.sub(grad.mult_const(learning_rate));
}

int main(int argc, char*argv[]) {
    int rank, err, nprocs;


    err = MPI_Init(&argc, &argv);
    check_mpi_error(err);

    err = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    check_mpi_error(err);
    err = MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    check_mpi_error(err);

    if (argc != 2) {
        throw std::runtime_error("./main <input_file>");
    }

    auto dataset = read_input(argv[1]);

    Tensor w(dataset.features());
    w.fill(1);
    double learning_rate = 0.001;

    auto start_time = get_time_us() / 1000;
    for (uint64_t i = 0; i < 100000000; ++i) {

        if (i && (i % 200 == 0)) {
            auto cost = calc_cost(dataset, w);
            //std::cout << "Iteration: " << i << std::endl;
            auto now = get_time_us() / 1000;
            std::cout << "Iteration: " << i 
                << " cost: " << cost 
                << " time: " << now-start_time
                << " time/iter: " << 1.0*(now-start_time)/i
                << std::endl;
        }

        //auto grad = calc_batch_grad(dataset, w);
        auto grad = calc_stochastic_grad(dataset, w);
        w = update_w(w, learning_rate, grad);
    }

    // we have these many samples
    // we have 100 features per sample
    //int samples = 1000;
    //int dimensions = 1000;

    //int* dataset = NULL;
    //int* rbuf = new int[1000];
    //if (rank == 0) {
    //    std::cout << "Master" << std::endl;
    //    dataset = new int[samples * dimensions];
    //    for (int i = 0; i < samples * dimensions; ++i)
    //        dataset[i] = i;

    //} else {
    //    std::cout << "Worker " << rank << std::endl;
    //}

//    MatrixXd weights = MatrixXd::Ones(dimensions, 1);
//
//    while (1) {
//        MPI_Scatter(sendbuf, 1000 / nprocs, MPI_INT, rbuf, 1000 / nprocs, MPI_INT, 0, MPI_COMM_WORLD);
//
//        for (int i = 0; i < 1000 / nprocs; ++i) {
//            std::cout << rank << " " << rbuf[i] << std::endl;
//        }
//    }

    MPI_Finalize();

    return 0;
}

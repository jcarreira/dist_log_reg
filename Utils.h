#ifndef _UTILS_H_
#define _UTILS_H_

#include <sys/time.h>
#include <sstream>

#define LOG2(X) ((unsigned) (8*sizeof (unsigned long long) - __builtin_clzll((X)) - 1)

inline
uint64_t get_time_us() {
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return 1000000UL * tv.tv_sec + tv.tv_usec;
}

template <class C>
C string_to(const std::string& s) {
    std::istringstream iss(s);
    C c;
    iss >> c;
    return c;
}

#endif // _UTILS_H_
